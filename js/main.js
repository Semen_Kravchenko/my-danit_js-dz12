//Теория
//1 вопрос
//Разница между функциями setTimeout() и setInterval() состоит в том, что функция setTimeout() отрабатывает ровно один раз, а функция setInterval() продолжает отрабатывать, снова и снова через одинаковые промежутки времени.
//2 вопрос
//Функция внутри setTimeout() отработает, только после завершения выполнения текущего кода, настолько быстро, насколько возможно.
//3 вопрос
//Чтобы не засорять планировщик...


//Задание
const wrapper = document.querySelector('.images-wrapper');
const btnStop = document.createElement('button');
const btnCont = document.createElement('button');
btnStop.textContent = 'Прекратить';
btnCont.textContent = 'Возобновить показ';
btnStop.classList.add('btn', 'btnStop');
btnCont.classList.add('btn', 'btnCont');
wrapper.append(btnStop);

const total_pics_num = 4;
const interval = 10000;
const time_out = .5;
let i = 0;
let newIndex = 0;


let timeout;
let timer;
let opacity = 100;

function fade_to_next() {
    opacity--;
    let k = i + 1;
    const image_now = document.getElementById('image_' + i);
    if (i === total_pics_num) k = 1;
    const image_next = document.getElementById('image_' + k);
    image_now.style.opacity = opacity/100;
    image_now.style.filter = 'alpha(opacity='+ opacity +')';
    image_next.style.opacity = (100-opacity)/100;
    image_next.style.filter = 'alpha(opacity='+ (100-opacity) +')';
    timeout = setTimeout("fade_to_next()", time_out);
    if (opacity === 1) {
        opacity = 100;
        clearTimeout(timeout);
    }
}

wrapper.addEventListener('click', (evt)=>{
    evt.preventDefault();
    const btnStopEvt = evt.target.closest('.btnStop');
    const btnContEvt = evt.target.closest('.btnCont');
    if(evt.target === btnStopEvt) {
        pause();
        evt.target.remove();
        check();
        wrapper.append(btnCont);
    }
    if(evt.target === btnContEvt) {
        mainFunc();
        evt.target.remove();
        wrapper.append(btnStop);
    }
});

function pause() {
    clearInterval(timer);
};

function check() {
    const img = document.querySelectorAll('.image-to-show');
    img.forEach((el)=>{
        if(el.hasAttribute('style') === false || el.style.opacity > 0.01) {
            newIndex = Number(el.dataset.num);
        }
    });
};

function mainFunc() {
    i = newIndex;
    timer = setInterval (function() {
        i++;
        if (i > total_pics_num) i = 1;
        fade_to_next();
    }, interval);
}

mainFunc();
